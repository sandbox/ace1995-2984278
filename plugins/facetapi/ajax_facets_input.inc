<?php

/**
 * @file
 * The facetapi_links and facetapi_checkbox_links widget plugin classes.
 */

/**
 * Widget that renders facets as a list of clickable links.
 *
 * Links make it easy for users to narrow down their search results by clicking
 * on them. The render arrays use theme_item_list() to generate the HTML markup.
 */
class FacetapiInput extends FacetapiWidget {

  /**
   * @var array
   */
  protected $idReplacers = [
    '_',
    ' ',
    ':',
  ];

  /**
   * Implements FacetapiWidget::execute().
   *
   * Transforms the render array into something that can be themed by
   * theme_item_list().
   *
   * @see FacetapiWidgetLinks::setThemeHooks()
   * @see FacetapiWidgetLinks::buildListItems()
   */
  public function execute() {
    $element = &$this->build[$this->facet['field alias']];
    $index_id = $this->build['#facet']['map options']['index id'];
    // TODO: Set settings condition for autocomplete.
    $text_field = [
      '#type' => 'textfield',
      '#name' => rawurlencode($this->settings->facet),
      '#autocomplete_path' => 'ajax/autocomplete/' . $index_id,
      '#attributes' => [
        'placeholder' => 'Model Name',
        'id' => ['facet-autocomplete'],
        'data-facet-name' => rawurlencode($this->settings->facet),
        'data-raw-facet-name' => $this->settings->facet,
        'data-facet-uuid' => $this->getAjaxFacetsUuid(),
      ],
      '#attached' => [
        'library' => [
          ['system', 'ui.autocomplete'],
        ],
        'js' => [
          drupal_get_path('module', 'ajax_facets_input') . '/js/input_autocomplete.js',
        ],
      ],
    ];

    foreach ($element as $value => $item) {
      // Respect current selection.
      if ($item['#active'] == 1) {
        $text_field['#value'] = $value;
      }
    }

    // We cannot use drupal_html_id to save the same id for each facet.
    $wrapper_id = $this->build['#attributes']['id'] . '-wrapper';

    $element = [
      '#markup' => '<div id="' . $wrapper_id . '">' . render($text_field) . '</div>',
    ];

    ajax_facets_add_ajax_js($this->facet);
  }

  protected function getAjaxFacetsUuid() {
    return str_replace($this->idReplacers, '-', $this->id)
      . '-'
      . str_replace($this->idReplacers, '-', $this->key);
  }
}
