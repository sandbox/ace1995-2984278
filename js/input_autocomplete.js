(function ($) {
  Drupal.ajax_facets_input = {};
  Drupal.behaviors.jqueryui_autocomplete = {
    attach: function (context, settings) {
      $('#facet-autocomplete')
        .once('customAutocomplete')
        .autocomplete({
          source: Drupal.settings.basePath + 'ajax/autocomplete/' + settings.facetapi.index_id,
          minLength: 2,
          delay: 300,
          select: function (e, ui) {
            Drupal.ajax_facets_input.processCustomInput(e, ui.item.value);
          }
        })
        .keyup(function (e) {
          if (e.keyCode === 13) {
            var $this = $('#facet-autocomplete');
            Drupal.ajax_facets_input.processCustomInput(e, $this.val());
          }
        });
    }
  };
  /**
   * Callback for onClick event for widget selectbox.
   */
  Drupal.ajax_facets_input.processCustomInput = function (e, value) {
    var $this = $('#facet-autocomplete');
    var facetName = $this.attr('name');
    // Init history.
    Drupal.ajax_facets.initHistoryState($this);
    // If facets are already defined in queryState.
    if (Drupal.ajax_facets.queryState['f']) {
      // Exclude all values for this facet from query.
      Drupal.ajax_facets.excludeCurrentFacet(facetName);

      /* Default value. */
      if ($this.val() === '') {
        delete Drupal.ajax_facets.queryState['f'][Drupal.ajax_facets.queryState['f'].length];
      } else {
        Drupal.ajax_facets.queryState['f'][Drupal.ajax_facets.queryState['f'].length] = facetName + ':' + value;
      }
    }

    Drupal.ajax_facets.sendAjaxQuery($this, true);
  };

}(jQuery));
